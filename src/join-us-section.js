import validate from "./email-validator.js";

class WebsiteSection extends HTMLElement {
  static observedAttributes = ["title", "description"];

  constructor() {
    super();
  }

  connectedCallback() {
    this.title = this.getAttribute("title");
    this.description = this.getAttribute("description");
    if (!this.hasAttribute("title") || !this.hasAttribute("description")) {
      this.setCustomValidity(
        'Both "name" and "description" attributes are required'
      );
    }

    let shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.innerHTML = `
      <style>
        .sdom{
            display: block;
            padding-top: 25px;
            margin-bottom: 50px;
            color: #aaa;
        }
      </style>
      <div>
        <slot name='inner content' class="sdom"> default text </slot>
      </div>
      <slot>
      <div></div>
      </slot>
    `;
  }

  get title() {
    return this.title;
  }

  set title(val) {
    if (val) {
      this.setAttribute("title", val);
    } else {
      this.setAttribute("title", "");
    }
  }
  get description() {
    return this.description;
  }

  set description(val) {
    if (val) {
      this.setAttribute("description", val);
    } else {
      this.setAttribute("description", "");
    }
  }
}

customElements.define("website-section", WebsiteSection);

function sectionCreator(sectionTitle, submitText, description) {
  let savedEmail = localStorage.getItem("email");

  let newSection = document.createElement("website-section");
  newSection.setAttribute("title", sectionTitle);
  newSection.setAttribute("description", description);
  newSection.classList.add("app-section__join-program");
  newSection.style.height = "436px";
  newSection.style.background = `url("assets/images/join-bg.webp") no-repeat center/cover`;

  let customItem = document.createElement("div");
  customItem.setAttribute("slot", "inner content");
  customItem.textContent = "form section";
  customItem.style.textAlign = "center";
  newSection.appendChild(customItem);

  // Head part of the section
  let title = document.createElement("h1");
  title.textContent = sectionTitle;
  Object.assign(title.style, {
    color: "#fff",
    "font-size": "48px",
    padding: 0,
    margin: 0,
  });

  let desc = document.createElement("p");
  desc.textContent =
    "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
  Object.assign(desc.style, {
    fontSize: "24px",
    color: "#fff",
  });

  let topDiv = document.createElement("div");
  Object.assign(topDiv.style, {
    "max-width": "360px",
    margin: "auto",
    "text-align": "center",
    "font-weight": "100",
    "padding-top": "100px",
  });
  topDiv.appendChild(title);
  topDiv.appendChild(desc);

  newSection.appendChild(topDiv);

  let formInput = document.createElement("input");
  formInput.name = "input";
  formInput.id = "input";
  formInput.placeholder = "Email";
  formInput.value = savedEmail;
  formInput.classList.add("app-section__button--join-program");
  Object.assign(formInput.style, {
    background: "rgba(255,255,255,0.2)",
    width: "360px",
    border: "none",
    outline: "none",
    padding: "9px",
    color: "white",
  });

  let formSubmit = document.createElement("button");
  savedEmail === "" || savedEmail === undefined || savedEmail === null
    ? (formSubmit.textContent = submitText)
    : (formSubmit.textContent = "UNSUBSCRIBE");

  Object.assign(formSubmit.style, {
    background: "#55C2D8",
    width: "auto",
    border: "none",
    padding: "5px 10px",
    "margin-left": "30px",
    "border-radius": "18px",
    outline: "none",
    color: "white",
    "letter-spacing": "2px",
    "font-size": "14px",
  });

  // shown form
  let formDiv = document.createElement("form");
  formDiv.addEventListener("submit", (e) => {
    e.preventDefault();
  });

  Object.assign(formDiv.style, {
    display: "flex",
    "justify-content": "center",
  });

  savedEmail === "" || savedEmail === undefined || savedEmail === null
    ? (formInput.style.display = "block")
    : (formInput.style.display = "none");

  let fetchInProgress = false;
  formSubmit.onclick = () => {
    let val = formInput.value;
    if (validate(val) && !fetchInProgress) {
      formSubmit.disabled = true;
      formSubmit.classList.add("btn-disabled");
      fetchInProgress = true;

      if (localStorage.getItem("formState") === "shown") {
        fetch("/api/subscribe", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ email: val }),
        })
          .then((e) => {
            if (!e.ok) {
              window.alert(`Error ${e.status} (${e.statusText})`);
            } else {
              localStorage.setItem("email", val);
              formInput.style.display = "none";
              formSubmit.textContent = "UNSUBSCRIBE";
              localStorage.setItem("formState", "hidden");
            }
          })
          .finally(() => {
            formSubmit.disabled = false;
            formSubmit.classList.remove("btn-disabled");
            fetchInProgress = false;
          });
      } else {
        fetch("/api/unsubscribe", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
        })
          .then((e) => {
            if (!e.ok) {
              window.alert(`Error ${e.status} (${e.statusText})`);
            } else {
              formInput.style.display = "block";
              localStorage.removeItem("email");
              formSubmit.textContent = submitText;
              formInput.value = "";
              localStorage.setItem("formState", "shown");
            }
          })
          .finally(() => {
            formSubmit.disabled = false;
            formSubmit.classList.remove("btn-disabled");
            fetchInProgress = false;
          });
      }
    }
  };

  newSection.appendChild(formDiv);
  formDiv.appendChild(formInput);
  formDiv.appendChild(formSubmit);

  // Mobile
  if (window.innerWidth < 768) {
    Object.assign(newSection.style, {
      background: `url("assets/images/join-bg-smol.webp") no-repeat center/cover`,
      "padding-bottom": "50px",
    });
    Object.assign(formDiv.style, {
      "flex-direction": "column",
      "align-items": "center",
    });
    formSubmit.style.marginTop = "40px";
  }

  function remove() {
    newSection.remove();
  }

  return {
    remove,
    newSection,
  };
}

function create(type) {
  switch (type) {
    case "standard":
      return sectionCreator(type, "SUBSCRIBE", "standard-section");
    case "advanced":
      return sectionCreator(
        "Join Our Advanced Program",
        "Subscribe to Advanced Program",
        "advanced-section"
      );
  }
}

export default create;
