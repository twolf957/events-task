const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

export default function (p) {
  for (const i of VALID_EMAIL_ENDINGS) {
    if (p != "" && p != null && p.endsWith(i)) return true;
  }
  return false;
}

export function validateAsync(email) {
  return new Promise((resolve, reject) => {
    for (const i of VALID_EMAIL_ENDINGS) {
      if (email != "" && email != null && email.endsWith(i)) resolve(true);
    }
    reject(false);
  });
}

export function validateWithThrow(email) {
  for (const i of VALID_EMAIL_ENDINGS) {
    if (email != "" && email != null && email.endsWith(i)) return true;
  }
  throw new Error("Provided Email is Invalid");
}

function validateWithLog(email) {
  for (const i of VALID_EMAIL_ENDINGS) {
    if (email !== "" && email !== null && email?.endsWith(i)) {
      console.log(true);
      return true;
    }
  }
  console.log(false);
  return false;
}

export const logFuncObj = {
  validateWithLog,
};
