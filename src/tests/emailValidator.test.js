import { expect } from "chai";
import validate, {
  validateAsync,
  validateWithThrow,
  logFuncObj,
} from "../email-validator";
import sinon from "sinon";

describe("validate() test", () => {
  it("testing if function returns false with no input", () => {
    expect(validate()).to.equal(false);
  });
  it("testing if function returns false with an empty string input", () => {
    expect(validate("")).to.equal(false);
  });
  it("testing if function returns false with the wrong string input", () => {
    expect(validate("sus@mail.ru")).to.equal(false);
  });
  it("testing if function returns true with gmail input", () => {
    expect(validate("asd@gmail.com")).to.equal(true);
  });
  it("testing if function returns true with outlook input", () => {
    expect(validate("asd@outlook.com")).to.equal(true);
  });
  it("testing if function returns true with yandex input", () => {
    expect(validate("asd@yandex.ru")).to.equal(true);
  });
});
function suppressConsoleLog() {
  const originalConsoleLog = console.log;
  console.log = () => {};
  afterEach(() => {
    console.log = originalConsoleLog;
  });
}
// log functions
describe("validate() with log test", () => {
  // const stub = sinon.stub(console, "log");
  // stub.callsFake(validate(stub.args[0]));

  let consoleLogStub;
  beforeEach(function () {
    suppressConsoleLog()
  });

  afterEach(function () {
    // consoleLogStub.restore();
  });

  it("testing if function returns false with no input", () => {
    expect(logFuncObj.validateWithLog()).to.equal(false);
    expect(console.log.called).to.be.undefined;
  });
  it("testing if function returns false with an empty string input", () => {
    expect(logFuncObj.validateWithLog("")).to.equal(false);
    expect(console.log.called).to.be.undefined;
  });
  it("testing if function returns false with the wrong string input", () => {
    expect(logFuncObj.validateWithLog("sus@mail.ru")).to.equal(false);
    expect(console.log.called).to.be.undefined;
  });
  it("testing if function returns true with gmail input", () => {
    expect(logFuncObj.validateWithLog("asd@gmail.com")).to.equal(true);
  });
  it("testing if function returns true with outlook input", () => {
    expect(logFuncObj.validateWithLog("asd@outlook.com")).to.equal(true);
  });
  it("testing if function returns true with yandex input", () => {
    expect(logFuncObj.validateWithLog("asd@yandex.ru")).to.equal(true);
  });

  // stub.restore();
});

describe("async test", () => {
  it("testing if function returns false with no input", async () => {
    let errorOccured = false;
    await validateAsync().catch((e) => {
      expect(e).to.equal(false);
      errorOccured = true;
    });
    expect(errorOccured).to.equal(true);
  });
  it("testing if function returns false with an empty string input", async () => {
    let errorOccured = false;
    await validateAsync("").catch((e) => {
      expect(e).to.equal(false);
      errorOccured = true;
    });
    expect(errorOccured).to.equal(true);
  });
  it("testing if function returns false with the wrong string input", async () => {
    let errorOccured = false;
    await validateAsync("sus@mail.ru").catch((e) => {
      expect(e).to.equal(false);
      errorOccured = true;
    });
    expect(errorOccured).to.equal(true);
  });
  it("testing if function returns true with gmail input", async () => {
    expect(await validateAsync("asd@gmail.com")).to.equal(true);
  });
  it("testing if function returns true with outlook input", async () => {
    expect(await validateAsync("asd@outlook.com")).to.equal(true);
  });
  it("testing if function returns true with yandex input", async () => {
    expect(await validateAsync("asd@yandex.ru")).to.equal(true);
  });
});

describe("error throw test", () => {
  it("testing if function returns false with no input", () => {
    let errorOccured = false;
    try {
      validateWithThrow();
    } catch (error) {
      errorOccured = true;
      expect(error).to.be.an.instanceOf(Error);
      expect(error.message).to.equal("Provided Email is Invalid");
    }
    expect(errorOccured).to.equal(true);
  });

  it("testing if function returns false with an empty string input", () => {
    let errorOccured = false;
    try {
      validateWithThrow("");
    } catch (error) {
      errorOccured = true;
      expect(error).to.be.an.instanceOf(Error);
      expect(error.message).to.equal("Provided Email is Invalid");
    }
    expect(errorOccured).to.equal(true);
  });
  it("testing if function returns false with the wrong string input", () => {
    let errorOccured = false;
    try {
      validateWithThrow("sus@mail.ru");
    } catch (error) {
      errorOccured = true;
      expect(error).to.be.an.instanceOf(Error);
      expect(error.message).to.equal("Provided Email is Invalid");
    }
    expect(errorOccured).to.equal(true);
  });
  it("testing if function returns true with gmail input", () => {
    expect(validateWithThrow("asd@gmail.com")).to.equal(true);
  });
  it("testing if function returns true with outlook input", () => {
    expect(validateWithThrow("asd@outlook.com")).to.equal(true);
  });
  it("testing if function returns true with yandex input", () => {
    expect(validateWithThrow("asd@yandex.ru")).to.equal(true);
  });
});
