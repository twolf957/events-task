import create from "./join-us-section.js";

import "./styles/style.css";

window.addEventListener("load", () => {
  const s1 = create("standard");

  document
    .getElementById("app-container")
    .insertBefore(s1.newSection, document.querySelector(".app-footer"));

  fetch("/api/community")
    .then((d) => d.json())
    .then((e) => {
      let parentDiv = document.querySelector(".app-card-holder")
      for(let i=0; i<parentDiv.children.length; i++) {
        parentDiv.querySelectorAll("img")[i].src = e[i].avatar
        parentDiv.querySelectorAll(".name")[i].textContent = e[i].firstName + " " + e[i].lastName
        parentDiv.querySelectorAll(".desc")[i].textContent = e[i].position
      }
    });
});
