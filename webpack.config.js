const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
const HtmlWebpack = require("html-webpack-plugin");

module.exports = {
  entry: "./src/main.js",
  output: {
    filename: "final.js",
    path: path.resolve(__dirname, "dist"),
  },
  plugins: [
    new HtmlWebpack({
      template: "./src/index.html",
    }),
    new CopyPlugin({
      patterns: [{ from: "./src/assets", to: "assets" }],
    }),
  ],
  devServer: {
    static: path.join(__dirname, "dist"),
    port: 9000,
    hot: true,
    proxy: {
      "/api": {
        target: "http://localhost:3000",
        changeOrigin: true,
        pathRewrite: { "/api": "" },
      },
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: "/(node_modules)/",
        use: {
          loader: "babel-loader",
          options: { presets: ["@babel/preset-env"] },
        },
      },
      {
        test: /\.css$/,
        use: [{ loader: "style-loader" }, { loader: "css-loader" }],
      },
    ],
  },
  mode: "development",
};
